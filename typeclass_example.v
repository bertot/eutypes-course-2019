Require Import Arith ZArith List Bool Psatz.

Class Eq (A : Type) := {
  eqf : A -> A -> bool
}.

Instance eq_bool : Eq bool :=
{ eqf x y := if x then y else negb y }.


Instance eq_nat : Eq nat :=
{ eqf := Nat.eqb}.

Instance eq_Z : Eq Z :=
 {eqf := Z.eqb}.

Instance eq_prod {A B : Type} {eqA : Eq A} {eqB : Eq B} : Eq (A * B) :=
  { eqf := fun p1 p2 => andb (eqf (fst p1) (fst p2)) (eqf (snd p1) (snd p2))}.

Compute eqf (3, true) (1 + 1, true).

Fixpoint eql (A : Type) (f : A -> A -> bool) (l1 l2 : list A) :=
  match l1, l2 with
    nil, nil => true
  | a :: tl1, b :: tl2 => andb (f a b) (eql A f tl1 tl2)
  | _, _ => false
  end.

Instance eq_list (A : Type) {eqA : Eq A} : Eq (list A) :=
  { eqf := eql A (fun x y : A =>  eqf x y) }.

Compute (eqf ((1, true)::nil) ((2, true)::nil)).
