\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\newcommand{\coqor}{{\tt\char'134/}}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqnot}{\mbox{\~{}}}

\setbeamertemplate{footline}[frame number]
\title{Proofs about programs}
\author{Yves Bertot}
\date{September 2019}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Performing proofs}
\begin{itemize}
\item Interactive loop
\item Start with {\tt Lemma {\sl name} : {\sl statement}.  Proof.}
\item Then apply commands that decompose the goal.
\item {\tt intros}, {\tt split}, {\tt exists}, {\tt left}, {\tt right},
{\tt auto}, {\tt rewrite}.
\item use {\tt apply} with existing theorems.
\item Special case for {\tt In}: {\tt simpl} or {\tt compute}
\item When finished, save using {\tt Qed.}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Several styles of proof}
Several styles of proof
\begin{itemize}
\item Navigating logical connectives
\item Reasoning about recursive programs
\item Reasoning about inductive properties
\item Reasoning with decision procedures
\end{itemize}
Each style calls for a different set of tools
\end{frame}
\begin{frame}[fragile]
\frametitle{Navigating logical connectives}
Use a menomonic table
\begin{itemize}
\item Connectives : \(\forall\), \(\exists\), \(\wedge\), \(\vee\), \(\neg\)
\item Distinguish between positive and negative occurrences\\
{\em in conclusion} or {\em in hypotheses}
\item Secondary connective: equivalence.
\item Make a special treatment case for equality
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Connective to tactic table}
\begin{tabular}{|c|c|c|c|c|}
\hline
& \(\rightarrow\){} & \(\forall\) & \coqand{} & \coqor{}\\
\hline
\hline
hypothesis H&{ apply H}&{ apply H }&{ destruct H }&{destruct H}\\
&&&{ as [H1 H2] }&{ as [H1 | H2]}\\
\hline
goal &{ intros H' }&{ intros x }&{ split }&{ left}\\
 & &&&{ right}\\
\hline
\end{tabular}
\begin{tabular}{|c|c|c|c|}
\hline
& \(\exists\) & = & \coqnot\\
\hline
\hline
hypothesis H&{ destruct H }&{ rewrite \(\leftarrow\) H }&{exfalso; apply H}\\
&as [x Px] &{ rewrite \(\rightarrow\) H}&\\
\hline
goal&{ exists \(e\) }&{ reflexivity }&{ intros H'}\\
\hline
\end{tabular}
\end{frame}
\begin{frame}[fragile]
\frametitle{Worked example}
\begin{small}
\begin{alltt}
Lemma ex0 : \(\forall\) A : Type, \(\forall\) P Q R : A -> Prop, \(\forall\) x : A,
  (\(\forall\) z, P z -> Q z) ->
  P x \coqand{} R x -> \(\exists\) y, Q y \coqand{} R y.
Proof.
intros A P Q R x pq pxrx.
\textcolor{blue}{1 subgoal
  A : Type
  P, Q, R : A -> Prop
  x : A
  pq : \(\forall\) z : A, P z -> Q z
  pxrx : P x \coqand{} R x
  ============================
  \(\exists\) y : A, Q y \coqand{} R y}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Worked example}
\begin{small}
\begin{alltt}
destruct pxrx as [px rx].
\textcolor{blue}{  x : P x
  rx : R x
  ============================
  \(\exists\) y : A, Q y \coqand{} R y}
exists x.
\textcolor{blue}{
  ============================
  Q x \coqand{} R x}
split.
\textcolor{blue}{  ============================
  Q x

subgoal 2 is:
 R x}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Worked example}
\begin{small}
\begin{alltt}
\textcolor{blue}{  pq : forall z : A, P z -> Q z
  px : P x
  rx : R x
  ============================
  Q x}
apply pq.
\textcolor{blue}{  ============================
  P x}
exact px.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Worked example}
\begin{small}
\begin{alltt}
\textcolor{blue}{  A : Type
  P, Q, R : A -> Prop
  x : A
  pq : forall z : A, P z -> Q z
  px : P x
  rx : R x
  ============================
  R x}
assumption.
\textcolor{blue}{No more subgoals.}
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}
\frametitle{Proofs about recursive functions}
\begin{itemize}
\item Follow the structure of the function in proofs 
\begin{itemize}
\item For recursive programs: \textcolor{blue}{induction} is probably needed
\end{itemize}
\item pattern-matching constructs: \textcolor{blue}{destruct}
\item Comparison between constructors: \textcolor{blue}{discriminate} and \textcolor{blue}{injection}
\item Do not forget searching for existing theorems!
\item Reshape statements modulo computation: \textcolor{blue}{simpl}, \textcolor{blue}{cbn}, \textcolor{blue}{replace ... with ...}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example with the sum of first integers}
\begin{small}
\begin{alltt}
Fixpoint sumn (n : nat) :=
  match n with 0 => 0 | S p => sumn p + (p + 1) end.

Lemma sumn_f n : sumn n * 2 = n * (n + 1).
Proof.
induction n as [ | p IH].
\textcolor{blue}{2 subgoals
  
  ============================
  sumn 0 * 2 = 0 * (0 + 1)

subgoal 2 is:
 sumn (S p) * 2 = S p * (S p + 1)}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example with the sum of first integers}
\begin{small}
\begin{alltt}
reflexivity.
\textcolor{blue}{1 subgoal
  
  p : nat
  IH : sumn p * 2 = p * (p + 1)
  ============================
  sumn (S p) * 2 = S p * (S p + 1)}
cbn [sumn].
\textcolor{blue}{  ============================
  (sumn n + (n + 1)) * 2 = S n * (S n + 1)}
Search ((_ + _) * _).
\textcolor{blue}{
Nat.mul_add_distr_r:
   forall n m p : nat, (n + m) * p = n * p + m * p}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example with the sum of first integers}
\begin{small}
\begin{alltt}
\textcolor{blue}{  ============================
  (sumn n + (n + 1)) * 2 = S n * (S n + 1)}
rewrite Nat.mul_add_distr_r.
\textcolor{blue}{  IH : sumn p * 2 = p * (p + 1)
  ============================
  sumn n * 2 + (n + 1) * 2 = S n * (S n + 1)}
rewrite IH.
\textcolor{blue}{  ============================
  n * (n + 1) + (n + 1) * 2 = S n * (S n + 1)}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example with the sum of first integers}
\begin{small}
\begin{alltt}
rewrite (Nat.mul_comm (n + 1)), <- Nat.mul_add_distr_r,
  !Nat.add_1_r, Nat.add_succ_r, Nat.add_1_r, Nat.mul_comm.
reflexivity.
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Using advanced tactics}
the contents of the previous slide can be replaced by a call to a tactic
called \textcolor{blue}{ring}.
\begin{alltt}
ring.
Qed.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Using libraries}
\begin{itemize}
\item Coq comes with existing libraries
\item Existing functions have theorems about them
\item Important command : {\tt Search}
\end{itemize}
\begin{alltt}
Search filter.
\textcolor{blue}{filter_In:
  forall (A : Type) (f : A -> bool) (x : A) (l : list A),
  In x (filter f l) <-> In x l \coqand{} f x = true}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{Examples of powerful libraries}
\begin{itemize}
\item Mathematical Components
\begin{itemize}
\item Designed for the 4-color theorem, the odd order theorem (Feit-Thompson)
\item Used for elliptic curves, reasoning about robots, combinatorics,
transcendance proofs
\end{itemize}
\item Coquelicot
\begin{itemize}
\item Real analysis: limits, derivatives, integrals, mathematical functions
\item Used for numerical computations (wave function, mathematical constants)
\end{itemize}
\item VST
\begin{itemize}
\item Reasoning about programs in C (in connection with Compcert)
\item Used for pointer data structures, security proofs
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Proofs about with inductive propositions}
\begin{itemize}
\item Proofs by induction
\item Key insight: \textcolor{blue}{inductive} structure of proofs
\item Elementary bricks: constructors
\item Repetition in subproofs (of the same predicate)
\item Leads to induction hypotheses
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example proof with inductive propositions}
\begin{small}
\begin{alltt}
Require Import Arith.

Inductive t_closure \{T : Type\} (R : T -> T -> Prop) :
  T -> T -> Prop :=
  tc1 : forall x y, R x y -> t_closure R x y
| tc_s : forall x y z, R x y -> t_closure R y z ->
  t_closure R x z.

Definition is_suc x y := y = S x.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example proof with inductive propositions}
\begin{small}
\begin{alltt}
Lemma clos_is_suc_lt x y : t_closure is_suc x y -> x < y.
Proof.
induction 1 as [x y base | x y z fst_step tc IH].
\textcolor{blue}{  x, y : nat
  base : is_suc x y
  ============================
  x < y

subgoal 2 (ID 35) is:
 x < z}
  unfold is_suc in base.
\textcolor{blue}{  base : y = S x
  ============================
  x < y
}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example proof with inductive propositions}
\begin{small}
\begin{alltt}
  rewrite base.
\textcolor{blue}{  ============================
  x < S x
}
  now apply Nat.lt_succ_diag_r.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example proof with inductive propositions}
\begin{small}
\begin{alltt}
\textcolor{blue}{  x, y, z : nat
  fst_step : is_suc x y
  tc : t_closure is_suc y z
  IH : y < z
  ============================
  x < z}
apply Nat.lt_trans with y.
  unfold is_suc in fst_step.
  rewrite fst_step.
  now apply Nat.lt_succ_diag_r.
exact IH.
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}
\frametitle{inversion}
\begin{itemize}
\item An inductive type may have a constructor of the form
  \textcolor{blue}{\tt A -> B}
\item If no other constructor can prove \textcolor{blue}{\tt B}
\item Any instance of \textcolor{blue}{\tt B} can only hold if 
  \textcolor{blue}{\tt A} hold
\item In practice, it feels like the implication is inverted
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Inversion example}
\begin{alltt}
Inductive i_even : nat -> Prop :=
  | ie0 : i_even 0
  | ie2 : forall m, i_even m -> i_even (S (S m)).
\end{alltt}
\begin{itemize}
\item A statement of the form \textcolor{blue}{\tt i\_even (S (S (S x)))}
cannot have been proved using constructor ie0.
\item constructor {\tt ie2} was used in such a way that {\tt m} was
  {\tt S x}
\item For the proof to be complete, {\tt i\_even m} was proved
\item So we can deduce {\tt i\_even (S x)}
\item Same reasoning to show that {\tt i\_even 1} is false.
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Inversion example}
\begin{small}
\begin{alltt}
Lemma i_even_inv5 : i_even 5 -> False.
Proof.
intros ev5.
inversion ev5 as [|n3 ev3 eq3].
\textcolor{blue}{  ev5 : i_even 5
  n3 : nat
  ev3 : i_even 3
  eq3 : n3 = 3
  ============================
  False}
inversion ev3 as [|n1 ev1 eq1].
\textcolor{blue}{  ev1 : i_even 1
  eq1 : n1 = 1
  ============================
  False}
\end{alltt}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Inversion example}
\begin{small}
\begin{alltt}
\textcolor{blue}{  ev1 : i_even 1
  eq1 : n1 = 1
  ============================
  False}
inversion ev1.
\textcolor{blue}{No more subgoals.}
Qed.
\end{alltt}
\end{small}
\end{frame}
\begin{frame}
\frametitle{Proofs by reflection}
\begin{itemize}
\item Reason on algorithms that perform proofs
\item Use the proved algorithms to perform proofs
\item Three stages
\begin{itemize}
\item Find a generic language to describe problems
\item Write proved programs about the language
\item Make Coq recognize instances
\end{itemize}
\item Example material: proofs by associativity (+ commutativity)
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
