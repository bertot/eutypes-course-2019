# Introductory course on Coq/Gallina

## abstract

Coq is a system to write programs and verify that they work as expected.

It is based on the *calculus of inductive constructions*, a variant of *Type Theory*.  It makes it possible to describe programs in three steps:

1. write how the program operates
2. write what the program is supposed to do as a logical statement
 between inputs and outputs
3. write a proof showing that the program really does what is specified

There are several proof systems that make it possible to perform these
  three steps.  What distinguishes Coq is a particular effort to
  support large scale efforts.  Coq can produce software that is used in real life.

This page collects the material for an introductory course on Coq
consisting of approximately 3 hours, plus material for exercises and
experiments.

A longer tutorial is available in [Coq in a hurry](https://cel.archives-ouvertes.fr/file/index/docid/459139/filename/coq-hurry.pdf)

## Programming

Describing simple functional programs in the gallina language

 - [slides](slides1.pdf)
 - examples ([sorting](sorting.v), [computing integers](integers.v))
 - [exercises](exercise1.v), [solutions](answers1.v)

## Logic and specification
- [slides](slides2.pdf)
- [examples](examples2.v)
- [exercises](exercise2.v), [solutions](answers2.v)

## Proving properties of programs
- [slides](slides3.pdf)
- examples: [proofs by reflection](example_reflection.v),
  [proofs of sorting algorithms](sorting_proofs.v)
- [exercises](exercise3.v), [solutions](answers3.v),
  [solution to the convolute exercise](convolute1.v),
  [solution to the question on irrational square roots](root_irrational.v)

## Advanced data-types

- slides
- examples [programming with dependent types](dep_prog.v)
- [exercises](exercise4.v), [solutions](answers4.v)
