# Instructions for making the material
## Main page

The main page is simply a html rendering of the README.md page.

To create the html file, I installed markdown in the `python` universe using the following command:

    pip3 install --user markdown

Then I generated the html file with the following command:

    python3 -m markdown README.md > index.html