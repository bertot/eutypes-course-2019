Require Import ZArith List sorting.

Open Scope Z_scope.

(* The following two function remove duplicates from a sorted list. *)
Fixpoint remove_duplicates_aux (a : Z) (l : list Z) : list Z :=
  match l with
  | b :: tl =>
    if a =? b then remove_duplicates_aux a tl
    else a::remove_duplicates_aux b tl
  | nil => a :: nil
  end.

Definition remove_duplicates (l : list Z) :=
  match l with
  | a :: tl => remove_duplicates_aux a tl
  | nil => nil
  end.

Compute length (remove_duplicates (tree_sort Z.leb sample2)).

Definition dup_sample2 := 
  Eval vm_compute in 
   let s := tree_sort Z.leb sample2 in
   remove_duplicates (map fst (filter (fun '(x, y) => x =? y) (zip s (30000::s)))).

Compute firstn 10 (rev dup_sample2).
