Require Import Arith ZArith Psatz List String.

(* This is a comment. *)
(* The next two blocks of text give an algorithm (sumn) *)
(* a statement about this algorithm (sumn n * 2 = n * (n + 1) *)
(* and a proof of this statement. (Proof. ... Qed.) *)
Fixpoint sumn (n : nat) :=
  match n with 0 => 0 | S p => sumn p + (p + 1) end.

Lemma sumn_prop (n : nat) : sumn n * 2 = n * (n + 1).
Proof.
induction n as [ | n IH].
  reflexivity.
cbn [sumn]; rewrite Nat.mul_add_distr_r, IH; ring.
Qed.

(* The next block shows a more elementary proof of the same
   statement. *)
Lemma sumn_prop2 (n : nat) : sumn n * 2 = n * (n + 1).
Proof.
induction n as [ | n IH].
  reflexivity.
cbn [sumn].
rewrite Nat.mul_add_distr_r. 
rewrite IH.
rewrite (Nat.mul_comm (n + 1)), <- Nat.mul_add_distr_r,
  !Nat.add_1_r, Nat.add_succ_r, Nat.add_1_r, Nat.mul_comm.
reflexivity.
Qed.

(* The next part does a similar study, but for another algorithm,
which computes the square root of a number in a stupid and naive way. *)
Fixpoint sqrt_aux (n p : nat) :=
  match n with
  | S k => if k ^ 2 <=? p then k else sqrt_aux k p
  | 0 => 0
  end. 

Definition sqrt (p : nat) := sqrt_aux (p + 1) p.

Lemma sqrt_correct p : sqrt p ^ 2 <= p < (sqrt p + 1) ^ 2.
Proof.
assert (tmp : p < (p + 1) ^ 2).
  replace ((p + 1) ^ 2) with ((p + 1) + (p + p ^ 2)) by (simpl; ring).
  apply Nat.lt_lt_add_r; lia.
assert (main : forall n, p < n ^ 2 -> (sqrt_aux n p) ^ 2 <= p <
              ((sqrt_aux n p) + 1) ^ 2).
  induction n as [ | n IH].
    now intros abs; inversion abs.
  intros ub. cbn [sqrt_aux]. case (n ^ 2 <=? p) eqn: test.
    split;[now apply leb_complete | now rewrite Nat.add_1_r].
  now apply IH, leb_complete_conv.
unfold sqrt.
now apply main.
Qed.

(* It takes 6 seconds to compute the square root of 1000 *)
Compute sqrt 1000.

(* It is possible to write a more efficient algorithm, that does
  not compute any square. *)
Fixpoint sqrt2_aux (n p : nat) : nat * nat :=
  match n with
  | 0 => (0, 0)
  | S n' => 
      match p with
      | 0 => (0, 0)
      | 1 => (1, 0)
      | 2 => (1, 1)
      | 3 => (1, 2)
      | _ => let q := p / 4 in
        let r := p mod 4 in
        let (v, r') := sqrt2_aux n' q in
        if (4 * v) + 1 <=? 4 * r' + r then
           (2 * v + 1, 4 * r' + r - (4 * v + 1))
        else (2 * v , 4 * r' + r)
      end
  end.

(* The following lemma is a symbolic execution tool, that makes
 it possible to step through the execution of the algorithm. *)
Lemma sqrt2_aux_step n p : sqrt2_aux n p =
  match n with
  | 0 => (0, 0)
  | S n' => 
      match p with
      | 0 => (0, 0)
      | 1 => (1, 0)
      | 2 => (1, 1)
      | 3 => (1, 2)
      | _ => let q := p / 4 in
        let r := p mod 4 in
        let (v, r') := sqrt2_aux n' q in
        if (4 * v) + 1 <=? 4 * r' + r then
           (2 * v + 1, 4 * r' + r - (4 * v + 1))
        else (2 * v , 4 * r' + r)
      end
  end.
Proof. now case n. Qed.

Definition sqrt2 n := fst (sqrt2_aux n n).

(* The proof of correctness for sqrt2 is exceedingly complex because
   subtraction of natural numbers is too irregular. *)

Lemma sqrt2_aux_correct n p v r :
  p <= n ->
  sqrt2_aux n p = (v, r) -> v ^ 2 <= p < (v + 1) ^ 2 /\ v ^ 2 + r = p.
Proof.
revert p v r; induction n as [ | n IH]; intros p v r condp.
  rewrite Nat.le_0_r in condp; rewrite condp; simpl.
  now intros h; injection h; intros v0 r0; rewrite <- v0, <-r0; lia.
rewrite sqrt2_aux_step; destruct p as [ | [ | [ | [ | k]]]] eqn: hp; intros h;
   try (injection h; clear h; intros vv vr; rewrite <- vv, <-vr; simpl; lia).
revert h; lazy zeta; rewrite <- hp.
set (q := p / 4); set (r' := p mod 4).
assert (pdiv4 : p = 4 * q + r').
  assert (d4 : 4 <> 0) by discriminate.
  now assert (tmp := Nat.div_mod p 4 d4); unfold q, r'.
assert (qlen : q <= n).
  assert (d4 : 4 <> 0) by discriminate.
  now assert (tmp := Nat.div_mod p 4 d4); unfold q; lia.
destruct (sqrt2_aux n q) as [v1 r1] eqn:h1.
assert (IH' := IH _ _ _ qlen h1).
destruct (4 * v1 + 1 <=? 4 * r1 + r') eqn:test; intros h.
  assert (vv1 : v = 2 * v1 + 1).
    injection h; intros rr1 vv1; rewrite <- vv1; ring.
  assert (rr1 : r = 4 * r1 + r' - (4 * v1 + 1)).
    injection h; intros rr1 vv'; rewrite <- rr1; lia.
  assert (v2rp : v ^ 2 + r = p).
    replace (v ^ 2 + r) with ((4 * v1 ^ 2 + 4 * v1 + 1) +
                              (4 * r1 + r') - (4 * v1 + 1)); cycle 1.
      rewrite vv1, rr1.
      replace ((2 * v1 + 1) ^ 2) with (4 * v1 ^ 2 + 4 * v1 + 1)
             by (simpl; ring).
      replace (4 * v1 ^ 2 + 4 * v1 + 1 + (4 * r1 + r')) with
          ((4 * v1 ^ 2) + (4 * r1 + r') + (4 * v1 + 1)) by ring.
      rewrite Nat.add_sub, <- !Nat.add_assoc; apply f_equal.
      rewrite Nat.add_assoc, le_plus_minus_r; auto.
      now apply leb_complete.      
    replace (4 * v1 ^ 2 + 4 * v1 + 1 + (4 * r1 + r')) with
      (4 * v1 ^ 2 + (4 * r1 + r') + (4 * v1 + 1)) by ring.
    rewrite Nat.add_sub, Nat.add_assoc, <- Nat.mul_add_distr_l, (proj2 IH').
    now rewrite <- pdiv4.
  split;[split; [rewrite <-v2rp; lia | ]| easy].
  assert (p4q4 : p < 4 * q + 4 * 1).
    rewrite pdiv4; apply Nat.add_lt_mono_l, Nat.mod_upper_bound; discriminate.
  apply (Nat.lt_le_trans _ _ _ p4q4); rewrite <- Nat.mul_add_distr_l.
  replace (v + 1) with (2 * (v1 + 1)) by (rewrite vv1; ring).
  rewrite Nat.pow_mul_l; apply Nat.mul_le_mono_l; auto.
  now rewrite Nat.add_1_r; exact (proj2 (proj1 IH')).
assert (vv1 : v = 2 * v1)
  by now (injection h; intros rr1 vv1; rewrite <-vv1).
assert (rr1 : r = 4 * r1 + r')
  by now (injection h; intros rr1 _; rewrite <- rr1).
assert (v2rp : v ^ 2 + r = p).
  rewrite vv1, rr1, pdiv4, <- (proj2 IH'); simpl; ring.
split;[split;[rewrite <- v2rp; lia | ] | easy].
apply leb_complete_conv in test.
rewrite pdiv4, <- (proj2 IH'), vv1.
replace ((2 * v1 + 1) ^ 2) with (4 * v1 ^ 2 + 4 * v1 + 1) by (simpl; ring).
now rewrite Nat.mul_add_distr_l, <- !Nat.add_assoc; apply Nat.add_lt_mono_l.
Qed.

(* Now, we can prove the right property for sqrt2. *)
Lemma sqrt2_correct n : sqrt2 n ^ 2 <= n < (sqrt2 n + 1) ^ 2.
Proof.
destruct (sqrt2_aux n n) as [v r] eqn: h.
destruct (sqrt2_aux_correct n n v r (le_n _) h) as [int val].
unfold sqrt2; rewrite h; exact int.
Qed.

(* We shall now do a similar proof in the type Z, where subtraction is
less irregular. *)
Open Scope Z_scope.

(* We can take advandage of the binary number encoding, where division
   by 4 appears naturally through pattern matching. *)

Fixpoint sqrtp (p : positive) : Z * Z :=
  let mkres := fun '(v, r) => 
    if 4 * v + 1 <=? 4 * r + Zpos p mod 4 then
      (2 * v + 1, 4 * r + Zpos p mod 4 - (4 * v + 1))
    else
      (2 * v, 4 * r + Zpos p mod 4) in
  match p with
  | xH => (1, 0)
  | xO xH => (1, 1)
  | xI xH => (1, 2)
  | xO (xO q) => mkres (sqrtp q)
  | xI (xO q) => mkres (sqrtp q)
  | xO (xI q) => mkres (sqrtp q)
  | xI (xI q) => mkres (sqrtp q)
  end.

Definition sqrtz (z : Z) :=
  match z with Zpos p => sqrtp p | _ => (0, 0) end.

(* Note that we are now able to compute square roots of much larger
   numbers. *)

Compute sqrtz 200000000000000000000.

Lemma  inj_pair (a b c d : Z) : (a, b) = (c, d) -> a = c /\ b = d.
Proof. now intros h; injection h; subst. Qed.

Lemma mod4II q : Zpos q~1~1 mod 4 = 3.
Proof.
rewrite 2?Pos2Z.pos_xI, Z.mul_add_distr_l, Z.mul_assoc, <- Z.add_assoc.
rewrite Z.add_comm, (Z.mul_comm (2 * 2)).
apply Z_mod_plus_full.
Qed.

Lemma mod4IO q : Zpos q~1~0 mod 4 = 2.
Proof.
rewrite Pos2Z.pos_xO, ?Pos2Z.pos_xI, Z.mul_add_distr_l, Z.mul_assoc.
rewrite Z.add_comm, (Z.mul_comm (2 * 2)).
apply Z_mod_plus_full.
Qed.

Lemma mod4OI q : Zpos q~0~1 mod 4 = 1.
Proof.
rewrite Pos2Z.pos_xI, (Pos2Z.pos_xO q), Z.mul_assoc.
rewrite Z.add_comm, (Z.mul_comm (2 * 2)).
apply Z_mod_plus_full.
Qed.

Lemma mod4OO q : Zpos q~0~0 mod 4 = 0.
Proof.
rewrite Pos2Z.pos_xO, (Pos2Z.pos_xO q), Z.mul_assoc.
rewrite (Z.mul_comm (2 * 2)).
apply Z_mod_mult.
Qed.

(* These proofs are still complex because the constructor structure does
  not make easy to abstract over the value of successive bits. *)
Lemma sqrtp_correct1 p v r :
  sqrtp p = (v, r) -> Zpos p = v ^ 2 + r.
Proof.
enough (main : forall n p v r,
             (Pos.to_nat p <= n)%nat -> sqrtp p = (v, r) -> Zpos p = v ^2 + r).
  now apply (main (Pos.to_nat p)).
clear p v r.
intros n; induction n as [ | n IH]; intros p;
  destruct p as [ [q2 | q2 | ] | [q2 | q2 | ] | ] eqn:peq; intros v r lep;
  try (solve[apply le_not_lt in lep; case lep; apply Pos2Nat.is_pos]);
  try (simpl; intros h; injection h as vq rq; rewrite <- vq, <- rq; easy);
  cbn [sqrtp]; destruct (sqrtp q2) as [v1 r1] eqn: hr; rewrite <- peq;
  destruct (4 * v1 + 1 <=? 4 * r1 + Z.pos p mod 4) eqn:test; intros h;
  apply inj_pair in h; destruct h as [vq rq]; rewrite <- vq, <- rq;
  (apply IH in hr;[ | apply le_S_n; apply le_trans with (2 := lep);
   rewrite ?Pos2Nat.inj_xI, ?Pos2Nat.inj_xO; lia ]); rewrite peq;
   rewrite ?mod4II, ?mod4IO, ?mod4OI, ?mod4OO, ?Pos2Z.pos_xI;
   set (a := 2); set (b := 4); rewrite 1?Pos2Z.pos_xO, ?Pos2Z.pos_xI; fold a;
   rewrite 1?Pos2Z.pos_xO; unfold a, b; rewrite hr; try ring.
Qed.

Lemma sqrtp_correct2 p v r :
  sqrtp p = (v, r) -> 0 <= r <= 2 * v.
Proof.
enough (main : forall n p v r,
             (Pos.to_nat p <= n)%nat -> sqrtp p = (v, r) -> 0 <= r <= 2 * v).
  now apply (main (Pos.to_nat p)).
clear p v r.
induction n as [ | n IH]; intros p;
  destruct p as [ [q2 | q2 | ] | [q2 | q2 | ] | ] eqn:peq; intros v r lep;
  try (solve[apply le_not_lt in lep; case lep; apply Pos2Nat.is_pos]);
  try (simpl; intros h; injection h as vq rq; rewrite <- vq, <- rq; easy);
  cbn [sqrtp]; destruct (sqrtp q2) as [v1 r1] eqn: hr; rewrite <- peq;
  destruct (4 * v1 + 1 <=? 4 * r1 + Z.pos p mod 4) eqn:test; intros h;
  apply inj_pair in h; destruct h as [vq rq]; rewrite <- vq, <- rq;
  (apply IH in hr;[ | apply le_S_n; apply le_trans with (2 := lep);
   rewrite ?Pos2Nat.inj_xI, ?Pos2Nat.inj_xO; lia ]); revert test rq;
   rewrite peq, ?mod4II, ?mod4IO, ?mod4OI, ?mod4OO, ?Pos2Z.pos_xI;
   intros test rq;
   set (a := 2); set (b := 4); rewrite 1?Pos2Z.pos_xO, ?Pos2Z.pos_xI; fold a;
   rewrite 1?Pos2Z.pos_xO; unfold a, b; try (apply Z.leb_gt  in test);
   try (apply Z.leb_le in test);
   try lia.
Qed.

(* Now we group the results for the function on positive numbers. *)
Lemma sqrtp_correct p v r :
  sqrtp p = (v, r) -> v ^ 2 <= Z.pos p < (v + 1) ^2.
Proof.
intros hc; generalize hc; intros hc'; apply sqrtp_correct1 in hc.
apply sqrtp_correct2 in hc'.
split.
  lia.
rewrite hc; replace ((v + 1) ^ 2) with (v ^ 2 + 2 * v + 1) by ring.
lia.
Qed.

(* we obtain the similar lemma for sqrtz. *)

Lemma sqrtz_correct z v r : 0 <= z -> sqrtz z = (v, r) ->
  v ^ 2 <= z < (v + 1) ^ 2.
Proof.
destruct z as [ | p | p].
    intros _; cbn [sqrtz]; intros h; destruct (inj_pair _ _ _ _ h) as [vq rq].
    now rewrite <- vq; lia.
  now intros _; apply sqrtp_correct.
now intros abs; case abs; compute.
Qed.

Fixpoint fib (n : nat) :=
  match n with S (S p as x) => fib x + fib p | _ => 1 end.

Fixpoint fib1 (a b : Z) (n : nat) :=
  match n with S p => fib1 b (b + a) p | _ => a end.

Lemma fib_step n : fib (S (S n)) = fib (S n) + fib n.
Proof. now case n. Qed.

Lemma fib1_step (a b : Z) (n : nat) :
  fib1 a b n = match n with S p => fib1 b (b + a) p | _ => a end.
Proof. now case n. Qed.

Lemma fib_fib1 (n p : nat) : fib (n + p) = fib1 (fib p) (fib (S p)) n.
Proof.
revert p; induction n as [ | n IHn].
  auto.
intros p; rewrite fib1_step, <- fib_step, <- IHn.
now rewrite Nat.add_succ_r, Nat.add_succ_l.
Qed.

Close Scope Z_scope.

Fixpoint sum_squares (n : nat) :=
  match n with
    O => 0
  | S p => S p ^ 2 + sum_squares p
  end.

Lemma sum_square_id n : 6 * sum_squares n = n * (n + 1) * (2 * n + 1).
Proof.
induction n as [ | n IHn].
  auto.
replace (sum_squares (S n)) with
  (S n ^ 2 + sum_squares n) by auto.
rewrite Nat.mul_add_distr_l, IHn; simpl; ring.
Qed.

Open Scope Z_scope.

Inductive Collatz : Z -> Prop :=
| c1 : Collatz 1
| c2 : forall n, n mod 2 = 0 -> Collatz (n / 2) -> Collatz n
| c3 : forall n, n mod 2 = 1 -> Collatz (3 * n + 1) -> Collatz n.

Lemma c7 : Collatz 27.
Proof.
repeat (apply c1 || (apply c2;
   [reflexivity | compute ]) || (apply c3; [reflexivity | compute])).
Qed.

Definition Collatz_f (x : Z) :=
  if x mod 2 =? 0 then (x / 2) else (3 * x + 1).

Fixpoint iterates f (x : Z) (n : nat) :=
  match n with
    0%nat => nil
  | S p => x :: iterates f (f x) p
  end.

Compute last (iterates Collatz_f 27 112) 27.

Lemma iter_Collatz_f_complete n :
  Collatz n -> exists k, In 1 (iterates Collatz_f n k).
Proof.
induction 1 as [ | n ar c IH| n ar c IH].
    exists 1%nat; simpl; auto.
  destruct IH as [k fd]; exists (S k); simpl; right.
  now unfold Collatz_f at 2; rewrite ar.
destruct IH as [k fd]; exists (S k); simpl; right.
now unfold Collatz_f at 2; rewrite ar.
Qed.

Open Scope string_scope.

Class Show (T : Type) := {show : T -> string}.

Instance showBool : Show bool :=
  { show := fun b => if b then "true" else "false" }.

Fixpoint show_list_aux {T : Type} `{Show T} (l : list T) :
  string :=
  match l with
    nil => "]"
  | e :: l' =>
     "; " ++ show e ++ show_list_aux l'
  end.

Definition show_list {T : Type} `{Show T} (l : list T) :=
  match l with
    nil => "[]"
  | a :: l' => "[" ++ show a ++ show_list_aux l'
  end.

Instance showList {T : Type} `{Show T} : Show _ :=
 {show := show_list}.

Compute show ((true::false::nil)::(true::nil)::nil::nil).
Compute (fun x : list (list bool) => show x).

Definition cstep (x : Z) : Z :=
  if Z.even x then x / 2 else (3 * x + 1).

Compute map
  (fun n => let z := Z.of_nat n in (z, iter z _ cstep 10)) (seq 0 6).

Class C_index (x : Z) (y : Z).

Instance C_1 : C_index 1 0.

Instance C_o {x n : Z} `{C_index (cstep x) n} : C_index x (n + 1).

Definition ctrigger (x : Z) {y : Z} `{C_index x y} := y.

Compute ctrigger 10.
(* Don't be mistaken in believing that we know how to write a function
   the termination of which is still undecided in Coq.
   ctrigger is actually a 3 argument function, and the third argument
   was computed at type inference time. *)

Set Printing Implicit.

Check ctrigger.
Compute ctrigger 26623.

Inductive bin := Node (t1 t2 : bin) | Leaf.
