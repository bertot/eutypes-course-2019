Require Import Arith ZArith List Psatz.

(* The following example addresses the need to produce a default element to access in
   a list.  The extra condition named c in this example, explains that there should
   not be a need for a default value. *)

(* We first write a function that explains that if we know how to build of non-empty
  length of elements of type A, then we can exhibit an element in A. *)
Definition non_empty_list_element {A : Type} (n : nat) (l : list A) (c : n < length l) : A.
destruct l as [ | a l'].
  exfalso.
  simpl in c; lia.
exact a.
Qed.

(* This potentially existing value of type A can then be used in a nth function that
  takes as extra guarantee the condition on the index for the nth element. *)
Definition nthS {A : Type} (x : nat) (l : list A) (c : x < length l) : A :=
  nth x l (non_empty_list_element x l c).

(* When a test occur in a function, the extra condition is verified in the branch,
  we need functions that tell us logical statements about what has been tested.  These
  are functions of type sumbool, written { _ } + { _ }.  Following historical
  tradition, these functions are named with the _dec suffix. *)

Check le_lt_dec.

(* This function is like nth, except that it does not require a default value in
  the type of the elements of the list, but only in the returned type. *)
Definition tested_access {A B : Type} (f : A -> B) (b : B) (l : list A) (n : nat) :=
  match le_lt_dec (length l) n with
  | left h => b
  | right h' => f (nthS n l h')
  end.

(* It may happen that what we test gives a different logical fact from the one
  required.  In this case, the type-checker is not happy and complains. *)
Fail Definition tested_access' {A B : Type} (f : A -> B) (b : B)
    (l : list A) (n : nat) :=
  match le_dec (length l) n with
  | left h => b
  | right h' => f (nthS n l h')
  end.

(* The Coq system helps users in gathering the logical problems that may occur
  in these cases. *)
Program Definition tested_access' {A B : Type} (f : A -> B) (b : B)
    (l : list A) (n : nat) :=
  match le_dec (length l) n with
  | left h => b
  | right h' => f (nthS n l _)
  end.
Next Obligation.
rewrite <- Nat.nle_gt.
assumption.
Defined.

(* When using a function the output of which is boolean, there is no logical
  information telling what has been learned when entering each branch, as
  made visible by the following error message. *)
Fail Definition tested_access2 {A B : Type} (f : A -> B) (b : B)
    (l : list A) (n : nat) :=
if n <? length l then f (nthS n l _) else b.

(* On the other hand, the program tool attempts to help by gathering the
  information corresponding to the boolean test.
  However, it does not work if we keep using the "if ... then ... else ..." 
  syntax. *)
Program Definition tested_access2 {A B : Type} (f : A -> B) (b : B)
    (l : list A) (n : nat) :=
match n <? length l with true => f (nthS n l _) | false => b end.
Next Obligation.
  rewrite <- Nat.ltb_lt. 
  apply eq_sym.
  assumption.
Qed.

Compute tested_access' Z.of_nat (-1)%Z (seq 0 3) 6.

Compute tested_access2 Z.of_nat (-1)%Z (seq 0 3) 6.

Program Definition cycle_access {A : Type} (l : list A) (c : 0 < length l)
  (n : nat) : {c | In c l} :=
  nthS (n mod length l) l _
.
Next Obligation.
  apply Nat.mod_upper_bound.
  rewrite Nat.neq_0_lt_0.
  assumption.
Qed.
Next Obligation.
  unfold nthS.
  apply nth_In.
  apply Nat.mod_upper_bound.
  rewrite Nat.neq_0_lt_0.
  assumption.
Qed.

Lemma seq_lgt0 {s x} : 0 < x -> 0 < length (seq s x).
Proof.
now rewrite seq_length.
Qed.

Compute proj1_sig (cycle_access (seq 0 42) (seq_lgt0 (Nat.lt_0_succ _)) 123).

(* Now we shall look at recursive programming with dependent types. *)

(* let us first introduce an example of function programmed in a recursive fashion
  with no use of dependent types. *)
Fixpoint addr x y :=
  match y with
  | 0 => x
  | S y' => S (addr x y')
  end.

(* We can associate to this function a lemma that basically expresses its specification. *)
Lemma addr_add x y : addr x y = x + y.
Proof.
revert x; induction y as [ | y' IH]; intros x.
  rewrite Nat.add_0_r.
  simpl.
  reflexivity.
rewrite Nat.add_succ_r, <- IH.
simpl.
reflexivity.
Qed.

(* using the companion theorem, we can construct a function that has a dependent type
  which collects the data and its specification. *)
Definition adds (x y : nat) : {z | z = x + y} :=
  exist (fun z => z = x + y) (addr x y) (addr_add x y).

(* The next part of the game is to write the function in such a way that the proof
  and the algorithm are given hand in hand, in one description of the recursive process.
  We will advance by looking at successive failures. *)
Fail Fixpoint addrs (x y : nat) : {z | z = x + y} :=
  match y return {z | z = x + y} with
    0 => exist (fun z => z = x + 0) x eq_refl
  | S x' => 
    exist addrs x' (S y) _ 
  end.

(* We know that x + 0 = x is provable, we wish this proof step to be included
  in the description of this function.  This is possible by using the eq_ind_r
  theorem (this theorem is invoked in tactic-based proofs when using rewrite) *)
Fail Fixpoint addrs (x y : nat) : {z | z = x + y} :=
  match y return {z | z = x + y} with
    0 => exist (fun z => z = x + 0) x
         (eq_ind_r (fun e => x = e) eq_refl (Nat.add_0_r x))
  | S x' => 
    addrs x' (S y)
  end.

(* Now we still want to use addrs, the unqualified value of type nat is the
  right one, but the justification needs some work. *)
Fail Fixpoint addrs (x y : nat) : {z | z = x + y} :=
  match y return {z | z = x + y} with
    0 => exist (fun z => z = x + 0) x
         (eq_ind_r (fun e => x = e) eq_refl (Nat.add_0_r x))
  | S y' => 
    match addrs x y' with
      exist _ v vP => exist (fun z => z = x + S y') (S v) _
    end
  end.

Fail Fixpoint addrs (f : forall (x y' v : nat) (vP: v = x + S y'), True)
  (x y : nat) : {z | z = x + y} :=
  match x return {z | z = x + y} with
    0 => exist (fun z => z = 0 + y) y eq_refl
  | S x' => 
    match addrs f x' (S y) with
      exist _ v vP => 
      exist (fun z => z = S x' + y) v (f x' y v vP)
    end
  end.

Fixpoint addrs0 (f : forall (x y' v : nat) (vP: v = x + y'), S v = x + S y')
  (x y : nat) : {z | z = x + y} :=
  match y return {z | z = x + y} with
    0 => exist (fun z => z = x + 0) x
         (eq_ind_r (fun e => x = e) eq_refl (Nat.add_0_r x))
  | S y' => 
    match addrs0 f x y' with
      exist _ v vP => 
      exist (fun z => z = x + S y') (S v) (f x y' v vP)
    end
  end.

Lemma helper x y' v : v = x + y' -> S v = x + S y'.
Proof.
now intros vv; rewrite vv, Nat.add_succ_r.
Qed.

Check addrs0 helper.

(* The same using Program Fixpoint: we only give the data content, the logical
  part is broken down into pieces (and discarded) for us. *)
Program Fixpoint addrs x y : {z | z = x + y} :=
  match y with
  | 0 => x
  | S y' => S (addrs x y')
  end.
Check addrs_obligation_1.
Check addrs_obligation_2.
Print addrs.
Compute addrs 3 5.
Compute proj1_sig (addrs 3 5).

(* A word of caution: in my (limited) experience, functions defined using Program
  are difficult to reason about.  You often need to make sure that all the properties
  you will ever be needing are recorded in the type of these functions. *)

(* exercise: reproduce the same construction, but for function add'
   (as found in exercise3.v) *)

