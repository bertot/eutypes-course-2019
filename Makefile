all : slides1.pdf slides2.pdf slides3.pdf index.html exercise1.v exercise2.v\
       exercise3.v answers1.v answers2.v answers3.v sorting.v integers.v \
       examples2.v convolute1.v example_reflection.v exercise4.v answers4.v\
       sorting_proofs.v root_irrational.v
	scp slides1.pdf slides2.pdf slides3.pdf index.html exercise1.v exercise2.v \
          exercise3.v answers1.v answers2.v answers3.v sorting.v integers.v \
          examples2.v convolute1.v example_reflection.v exercise4.v answers4.v \
          sorting_proofs.v root_irrational.v\
          nardis.inria.fr:www/courses/eutypes-course-2019/

index.html : README.md
	python3 -m markdown --extension=sane_lists README.md > index.html

slides1.pdf : slides1.tex
	pdflatex slides1

slides2.pdf : slides2.tex
	pdflatex slides2

slides3.pdf :slides3.tex
	pdflatex slides3

sqrt_algo.pdf : sqrt_algo.svg
	inkscape sqrt_algo.svg --export-pdf=sqrt_algo.pdf
