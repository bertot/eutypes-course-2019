\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\newcommand{\coqor}{{\tt\char'134/}}
\newcommand{\coqand}{{\tt/\char'134}}
\newcommand{\coqnot}{\mbox{\~{}}}

\setbeamertemplate{footline}[frame number]
\title{Logic and specification in Coq}
\author{Yves Bertot}
\date{September 2019}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Expressing properties of programs}
\begin{itemize}
\item Expressing properties of the output
\item Expressing a relation between the input and the output
\item Expressing properties that are {\em always} satisfied
\item Properties are similar to tests
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Starting from tests}

\begin{itemize} 
\item Tests rely on two components
\begin{itemize}
\item A piece of code to generate test cases
\item A piece of code to verify correct behavior on all cases
\end{itemize}
\item In our approach, we use logic
\begin{itemize}
\item to describe what are all possible inputs
\item to express what is the correct behavior
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\frametitle{Example on the filter function}
\begin{itemize}
\item What do you expect from the filter function?
\end{itemize}
\begin{alltt}
Fixpoint my_filter \{T : Type\} (p : T -> bool)
    (l : list T) : list T :=
match l with
  nil => nil
| a :: l1 =>
  if p a then a :: my_filter p l1 else my_filter p l1
end.
\end{alltt}
\end{frame}

\begin{frame}[fragile]
\frametitle{Filter function specification}
\begin{itemize}
\item The output of the filter function must only contain values satisfying
the boolean property
\item All values satisfying the boolean property should be taken
\pause
\item The multiplicity of values is preserved
\item The order of values is preserved
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\frametitle{Expressing a logical property}


\begin{itemize}
\item Difficulty of Coq: logical values are not boolean

\item In a sense, {\tt bool} is restricted to logical statements that
can be decided
\begin{itemize}
\item For instance, if \(f\) and \(g\) are functions on {\tt nat}, the
  fact that coincide everwhere cannot be decided
\end{itemize}
\item A new type {\tt Prop} is used for logical propositions

\item For {\tt T} a type and {\tt a b~:~T}, {\tt a = b~:~Prop}

\item {\tt Prop} also has connectives {\tt and} (\coqand{}), {\tt or}
(\coqor{}), {\tt not} (\coqnot{}), implication is written {\tt ->}

\item Universal quantification is written {\tt forall x : T, P x}
\item Existential quantification {\tt exists x : T, P x}
\item A boolean value \(v\) can be mapped to a proposition by writing
{\tt \(v\) = true}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Propositions for lists and numbers}
\begin{itemize}
\item {\tt In x l} is defined by a recursive computation that
yields a proposition based on {\tt =} and \coqor{}
\item Numbers have {\tt <=}, {\tt <}
\end{itemize}
\begin{alltt}
Compute In (2 + 3) (3 :: 5 :: 8 :: nil).
\textcolor{blue}{= 3 = 5 \coqor{} 5 = 5 \coqor{} 8 = 5 \coqor{} False : Prop}

Compute 2 <= 3.
\textcolor{blue}{= 2 <= 3 : Prop}

Compute 2 <=? 3.
\textcolor{blue}{= true : bool}

\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Specifications for {\tt filter}}
\begin{alltt}
forall (A : Type) (f : A -> bool) (x : A) (l : list A),
  In x (myfilter f l) -> In x l \coqand{} f x = true

forall (A : Type) (f : A -> bool) (x : A) (l : list A),
  In x l \coqand{} f x = true -> In x (myfilter f l)
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{specifications based on tests}
\begin{itemize}
\item Write your function: {\tt f~:~A -> B}
\item Write extra functions to verify that the output is correct,
{\tt verif~:~B -> bool}
\item Express a universal statement
{\tt forall x~:A, verif (f x) = true}
\item Being able to prove such a statement is equivalent to exhaustive testing.
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example: stating that a list is sorted}
\begin{alltt}
  Fixpoint is_sorted \{T : Type\} (R : T -> T -> bool)
    (l : list T) : bool :=
  match l with
    a :: (b :: _ as tl) =>
    if R a b then is_sorted tl else false
  | _ => true
  end.
\end{alltt}
A {\em partial} specification for a sort function is
\begin{alltt}
forall l, is_sorted (sort R l) = true
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Non computable properties}
\begin{itemize}
\item Type theory is strong enough to describe non-decidable properties
\item Example : Collatz (aka. Syracuse) sequences.
\begin{alltt}
Inductive Collatz : Z -> Prop :=
| c1 : Collatz 1
| c2 : forall n, n mod 2 = 0 -> Collatz (n / 2) ->
       Collatz n
| c3 : forall n, n mod 2 = 1 -> Collatz (3 * n + 1) ->
       Collatz n.
\end{alltt}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Inductive properties}
\begin{itemize}
\item Describe sets that are stable modulo some operations
\item Take the least set satisfying these operations.
\item Suitable for a many applications
\begin{itemize}
\item semantics of programming language
\item Describing grammars
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example: a grammar as an inductive property}
\begin{alltt}
Require Import String.

Open Scope string_scope.

Inductive wfpar : string -> Prop :=
  wf0 : wfpar EmptyString
| wfcat : forall s1 s2, wfpar s1 -> wfpar s2 -> 
          wfpar (s1 ++ s2)
| wfadd : forall s1, wfpar s1 ->
          wfpar ("(" ++ s1 ++ ")").
\end{alltt}
\begin{itemize}
\item Be careful about the sense in which one reads arrows
\item When describing a process (e.g. parsing), work follows arrows in reverse
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example: transitive closure as an inductive property}
\begin{alltt}
Inductive t_closure \{T : Type\} (R : T -> T -> Prop) :
  T -> T -> Prop :=
  tc1 : forall x y, R x y -> t_closure R x y
| tc_s : forall x y z, R x y -> t_closure R y z ->
  t_closure R x z.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example: transitive closure, take 2}
\begin{alltt}
Inductive t_closure2 \{T : Type\} (R : T -> T -> Prop) :
  T -> T -> Prop :=
  tc2_1 : forall x y, R x y -> t_closure2 R x y
| tc2_s : forall x y z,
  t_closure2 R x y -> t_closure2 R y z ->
  t_closure2 R x z.
\end{alltt}
\end{frame}
\begin{frame}[fragile]
\frametitle{Induction principle for an inductive property}
Every relation {\tt P} that satisfies the same stability property
as {\tt t\_closure} is a consequence.
\begin{alltt}
About t_closure_ind.
\textcolor{blue}{t_closure_ind :
forall (T : Type) (R P : T -> T -> Prop),
(forall x y : T, R x y -> P x y) ->
(forall x y z : T, R x y ->
            t_closure R y z -> P y z -> P x z) ->
forall y y0 : T, t_closure R y y0 -> P y y0}
\end{alltt}
\end{frame}
\begin{frame}
\frametitle{The power of inductive properties}
\begin{itemize}
\item Logical connectives
\item In Coq, logical connectives, equality, comparison between natural numbers are inductive properties
\item Beware of excessive use
\item Mathematical Components advocates a different style
\begin{itemize}
\item Express as much as possible using boolean functions
\item Provide bridges from {\tt bool} to {\tt Prop}
\end{itemize}
\end{itemize}
\end{frame}
\end{document}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
